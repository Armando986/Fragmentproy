package com.example.administrador.fragmentproy;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

/**
 * Created by administrador on 13/05/15.
 */
public class FragmentA extends Fragment implements OnClickListener {

    Button button;
    int counter=0;
    Communicator comm;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_a,container,false);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(savedInstanceState==null){
            counter=0;
        }
        else{
            counter=savedInstanceState.getInt("counter",0);
        }

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        comm=(Communicator)getActivity();
        button = (Button)getActivity().findViewById(R.id.buttonA);

        button.setOnClickListener(this);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putInt("counter",counter);
    }

    @Override
    public void onClick(View v) {

        counter++;
        comm.respond("el button es prisado "+counter+" veces");
    }
}
