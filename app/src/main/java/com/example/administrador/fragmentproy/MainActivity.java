package com.example.administrador.fragmentproy;

import android.app.FragmentManager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;


public class MainActivity extends ActionBarActivity implements Communicator {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);


    }

    public void respond(String data) {

        FragmentManager manager=getFragmentManager();
        FragmentB f2=(FragmentB)manager.findFragmentById(R.id.fragmentB);
        f2.changeText(data);

    }
}
